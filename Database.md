# The encrypted Database file

The database file `harbour-whisperfish.db` located in `~/.local/share/harbour-whisperfish/db` is an encrypted SQLite database, using [SQLCipher](https://github.com/sqlcipher/sqlcipher).

## Database encryption key
To access the data stored in this database, you need both your personal whisperfish password and the 8 byte `salt` data that is contained in the `~/.local/share/harbour-whisperfish/db` directory.
Together, they give rise to a 32 byte hash value obtained using the [`scrypt` algorithm](https://en.wikipedia.org/wiki/Scrypt), which is then used as the encryption key for the database.

Whisperfish uses default values to generate the scrypt hash, i.e. `N = 16384 = 2^14`, `r = 8`, and `p = 1`.

### Python code snippet to generate the key
The simplest way to generate the encryption key is to use one of the two Python packages `hashlib` or `scrypt`:

#### `hashlib`:
```python
import hashlib

password = b'<YOURPASSWORD>'
saltfile = "~/.local/share/harbour-whisperfish/db/salt"

with open(saltfile, "rb") as f:
    salt = f.read()
    print(hashlib.scrypt(pwb, salt=salt, n=1 << 14, r=8, p=1, maxmem=0, dklen=32).hex())
```
#### `scrypt`:
```python
import scrypt

password = b'<YOURPASSWORD>'
saltfile = "~/.local/share/harbour-whisperfish/db/salt"

with open(saltfile, "rb") as f:
    salt = f.read()
    print(scrypt.hash(pwb, salt, buflen = 32).hex())
```

## Database access
You can now access your data, e.g. with `sqlcipher`, `sqlitebrowser`, or any other SQLcipher-compatible tool.
The whisperfish database uses HMAC-SHA1, the default in `sqlciper` format version 3.
Please note, that, at the time of testing (SQLCipher 4.4.3 community), also the [KDF algorithm](https://www.zetetic.net/sqlcipher/sqlcipher-api/#cipher_kdf_algorithm) has to be set to PBKDF2_HMAC_SHA1, even though we will already provide a raw key.

Finally, you only need to use the hexadecimal hash value as generated above and set the corresponding cipher parameters.

For instance, `sqlcipher` requires one to use the format `x'<hex key>'` to indicate that the key is already in hexadecimal format and must not be salted any further, see also [here](https://www.zetetic.net/sqlcipher/sqlcipher-api/#key).
In `sqlitebrowser`, the key has to be entered in *raw* format and prepended by a `0x`.

### Dump plaintext data
```
sqlcipher ~/.local/shared/harbour-whisperfish/db/harbour-whisperfish.db
sqlite> PRAGMA key = "x'1234...'";
sqlite> PRAGMA cipher_page_size = 4096;
sqlite> PRAGMA cipher_hmac_algorithm = HMAC_SHA1;
sqlite> PRAGMA cipher_kdf_algorithm = PBKDF2_HMAC_SHA1;
sqlite> ATTACH DATABASE 'plaintext.db' AS plaintext KEY '';  -- empty key will disable encryption
sqlite> SELECT sqlcipher_export('plaintext');
sqlite> DETACH DATABASE plaintext;
```
