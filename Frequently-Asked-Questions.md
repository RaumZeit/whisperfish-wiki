# Registration

## Can I migrate from Signal Android to Whisperfish?

**Yes**, under a few conditions:

1. [V2 groups](https://gitlab.com/rubdos/whisperfish/-/issues/86) will work, but only if you're added to a group.  [GroupV2 *invites* and some other features](https://gitlab.com/groups/whisperfish/-/epics/1) are not yet implemented.
2. You don't mind that Whisperfish will not [import any Android messages](https://gitlab.com/rubdos/whisperfish/-/issues/15), specifically, Whisperfish will be a *new registration*.
3. You need to turn-off the registration lock in the Android Signal settings. Cfr. https://gitlab.com/rubdos/whisperfish/-/issues/148 and https://gitlab.com/rubdos/whisperfish/-/issues/149

## I get a white screen right after entering my phone number/402 Payment Required

Signal requires you to file a CAPTCHA, but SailfishOS 4.0 has a bug rendering it.
Use [this WebView patch](https://openrepos.net/node/11303/) or upgrade to SailfishOS 4.1.

## Whisperfish crashes right after filing the CAPTCHA

You *probably* come from Signal Android or iOS with your phone number,
and you have registration lock enabled.

[We currently don't catch this error](https://gitlab.com/whisperfish/whisperfish/-/issues/148).
There are two work arounds:
- Wait for seven days (don't use your other Signal client); or
- Disable "registration lock" in the settings of Signal Android/Signal iOS.

## I'm not able to login/WebSocket is disconnected.

You probably re-registered with an Android or iOS device.
See the next point on how to reset; and note that [this will disable the Android/iOS device](https://gitlab.com/whisperfish/whisperfish/-/issues/172).

## How do I reset Whisperfish to be able to register again?

[Resetting from the user interface](https://gitlab.com/whisperfish/whisperfish/-/issues/155) is not yet possible,
but you can run this command:

```sh
rm -rf ~/.local/share/harbour-whisperfish ~/.config/harbour-whisperfish
```

... which will remove all your current messages and contacts,
after which you will be able to re-register.
If you had daemon-mode turned on, reboot your device after running the `rm` command.

# Miscellaneous

## I received a voice note, but the media player does not play it.

The media player is confined to its own jail, and it cannot access Whisperfish' files.
The work-around is to change your `.config/harbour-whisperfish/harbour-whisperfish.conf`,
and change the attachment path to `~/Downloads/Whisperfish-attachments` or something like that,
which is accessible by the media player.
After that change, all newly received attachments will end up in that directory instead,
and will be accessible by the media player.
In practice, we want to [implement our own player inside the application](https://gitlab.com/whisperfish/whisperfish/-/issues/289);
if you would like that, please give that issue an upvote.
At that point, you are probably also interested in [sending your own voice notes](https://gitlab.com/whisperfish/whisperfish/-/issues/252).

## I get "Untrusted identity" in the log and don't receive any messages from this contact any more.

```
[ERROR harbour_whisperfish::worker::client] Error sending message: protocol error: internal error: Untrusted identity
```

Most probably, the party at the other side has linked a secondary device,
and for some (yet unknown) reason, this messes up the identity keys.
Make sure this is the case (ask them whether they did that recently), then proceed.

The relevant issues are https://gitlab.com/whisperfish/whisperfish/-/issues/353 (currently
confidential) and https://gitlab.com/whisperfish/whisperfish/-/issues/304.

What you should do, is run Whisperfish in verbose mode
(`harbour-whisperfish --verbose`), then send the concerning contact a
1:1 message (which won't come through, most probably). This will log
their UUID (of the form `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`).
If you see no log, it's because Whisperfish is still running in background.
From the terminal, kill the process (with the command `killall harbour-whisperfish`)
and run Whisperfish in verbose mode.

Then you should remove the identity file for that UUID and their phone
number: go into `.local/share/harbour-whisperfish/storage/identity`,
and remove the files related to the phone number (if it exists) and the UUID.

Then cross your fingers 🤞, send them another message, and hope things work
out.

## I have heard Signal bans people that use Whisperfish

The people behind Signal have been quite outspoken against third-party clients in the past (see e.g. [this May 2016 comment](https://github.com/LibreSignal/LibreSignal/issues/37#issuecomment-217211165)).  That said, they seem to have changed their mind shorty after, and seem to *tolerate* us, but *ignore* most (all?) outreach.

I, Ruben, have been quite open about the existence of Whisperfish.  We have our own very specific `User-Agent` (even including a version number) when talking to the OWS servers.  I have made our existence clear on their Github and on Reddit.  Although we have yet to hear any feedback, we tend to believe that they tolerate our presence.

This does *not* take away that they can ban any of us, when deemed necessary.  I kindly invite mr. Marlinspike and his colleagues for a friendly chat on Matrix, Libera.Chat, or - of course - Signal!

I'd like to put things in perspective here: WhatsApp has a *whitelist* on client `User-Agent`s.
People that develop third-party clients for WhatsApp have to do this in secret, while I do all of this with my name tag present.
Signal can trivially implement a whitelist too, and it seems like a deliberate choice of Signal *not to do this*.
That said, they have the ability and most probably even the right to ban us at any given notice.

## How can I help or contribute?

There are *many* ways how you can contribute!

1. Report issues, request features, preferably [an issue on Gitlab](https://gitlab.com/rubdos/whisperfish/-/issues),
but feel free to come on [Matrix or Libera.Chat](Contact-Us),
or [send me an email, Signal or text message](https://www.rubdos.be/about/) too.
2. Translations! ![https://hosted.weblate.org/engage/whisperfish/](https://hosted.weblate.org/widgets/whisperfish/-/whisperfish-application/svg-badge.svg) Have a look at our [translations guide](Translations), and [get in
   touch](Contact-Us) if you need help.
   The badges on our repository show how far we have translated already!
3. Writing code.  If you attempt to compile, come have a chat via [Matrix or Libera.Chat](Contact-Us), it's not a simple undertaking.
